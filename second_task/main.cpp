#include <iostream>
#include <vector>
#include <map>
struct ListNode {
     ListNode *      prev;
     ListNode *      next;
     ListNode *      rand; // указатель на произвольный элемент данного списка либо NULL
     std::string     data;
};

class List {
public:
     void Serialize   (FILE * file){
        std::map<ListNode*,int>  indices;
        indices[NULL]=0;

        ListNode* curr = head;
        
        for(int i=1;i<=count;i++){
            indices[curr]=i;
            curr=curr->next;
        }

        curr = head;
        fwrite(&count,sizeof(int),1,file);

        for(int i=0;i<count;i++){
            if(curr->rand)
                fwrite(&indices[curr->rand],sizeof(int),1,file);
            else
                fwrite(&indices[NULL],sizeof(int),1,file);

            size_t size =curr->data.size();
            fwrite(&size,sizeof(size_t),1,file);
            fwrite(curr->data.data(),sizeof(char),curr->data.size(),file);

            curr=curr->next;
        }
     };  // сохранение в файл (файл открыт с помощью fopen(path, "wb"))
     void Deserialize (FILE * file){

        fread(&count,sizeof(int),1,file);
        std::vector<ListNode*> list;

        for(int i=0;i<count;i++){
            list[i]=new ListNode;
        }

        for(int i=0;i<count;i++){
            int rand_index=0;
            fread(&rand_index,sizeof(int),1,file);
            list[i]->rand=(rand_index!=0)?list[rand_index-1]:NULL;
            list[i]->prev=(i!=1)?list[i-1]:NULL;
            list[i]->next=(i!=count)?list[i+1]:NULL;

            size_t size = 0;
            fread(&size,sizeof(size_t),1,file);

            std::string data(size,'0');
            fread(data.data(),sizeof(char),size,file);
            list[i]->data=data;
        }

        head=list[0];
        tail=list[count-1];
     };  // загрузка из файла (файл открыт с помощью fopen(path, "rb"))

private:
     ListNode *      head;
     ListNode *      tail;
     int       count;
};


int main(){

};