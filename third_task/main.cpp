#include <iostream>

struct vec3{
    float x,y,z;
};

vec3 normalize(const vec3& n){
    vec3 normalized;
    float norm_multiplier=sqrtf(n.x*n.x+n.y*n.y+n.z*n.z);

    normalized.x=n.x/norm_multiplier;
    normalized.y=n.y/norm_multiplier;
    normalized.z=n.z/norm_multiplier;

    return normalized;
}

vec3 cross_product(const vec3& a, const vec3& b){
    vec3 res;

    res.x = a.y*b.z - a.z*b.y;
    res.y = a.z*b.x - a.x*b.z;
    res.z = a.x*b.y - a.y*b.x;

    return res;

}

void calc_mesh_normals(vec3* normals, const vec3* verts, const int* faces, int nverts, int nfaces) 
{
    // Assuming all vertices in faces are in CW or CCW order
    for(int i=0;i<nfaces;i++){
        vec3 a;
        a.x = verts[faces[i*3+0]].x - verts[faces[i*3+2]].x;
        a.y = verts[faces[i*3+0]].y - verts[faces[i*3+2]].y;
        a.z = verts[faces[i*3+0]].z - verts[faces[i*3+2]].z;

        vec3 b;
        b.x = verts[faces[i*3+1]].x - verts[faces[i*3+2]].x;
        b.y = verts[faces[i*3+1]].y - verts[faces[i*3+2]].y;
        b.z = verts[faces[i*3+1]].z - verts[faces[i*3+2]].z;

        vec3 n = cross_product(a,b);

        normals[faces[i*3+0]].x += n.x;
        normals[faces[i*3+0]].y += n.y;
        normals[faces[i*3+0]].z += n.z;

        normals[faces[i*3+1]].x += n.x;
        normals[faces[i*3+1]].y += n.y;
        normals[faces[i*3+1]].z += n.z;

        normals[faces[i*3+2]].x += n.x;
        normals[faces[i*3+2]].y += n.y;
        normals[faces[i*3+2]].z += n.z;
    }

    for(int i=0; i<nverts;i++){
        normals[i]=normalize(normals[i]);
    }
}
int main(){
    //TODO:...
}