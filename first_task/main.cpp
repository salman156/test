#include <iostream>
#include <string>

template <typename T>
std::string to_bin(T number){
    constexpr int len = sizeof(number) * 8 -1;
    std::string bin(len+1,0);

    for(int i=len;i>=0;i--){
        bin[i]=(number&1)+'0';
        number>>=1;
    }

    return bin;
} 


int main(){
    std::cout<<to_bin(400234000i32)<<std::endl;
    std::cout<<to_bin(4004350000ui32)<<std::endl;
    std::cout<<to_bin(-400000000i32)<<std::endl;
    std::cout<<to_bin(4000000000i64)<<std::endl;
    std::cout<<to_bin(16000000000000000000ui64)<<std::endl;
    std::cout<<to_bin(-4000000000i64)<<std::endl;
    return 0;
};
